#ifndef ONDE_HPP
#define ONDE_HPP

class Onde {
public:
    Onde(double x, double y, double t);
    double hauteur() const;
private:
    double x_, y_, t_;
    static const int N = 5;
    double A_[N], T_[N], c_[N], phi_[N], theta_[N];
};

#endif // ONDE_HPP