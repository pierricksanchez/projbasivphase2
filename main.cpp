#include "ondes.hpp"
#include <iostream>
#include <chrono>
#include <thread>
#include <cmath>
#include <cstdlib>
#include <random>

int main()
{
    double x, y, t;
    std::cout << "Entrez les coordonnees (x, y, t) : ";
    std::cin >> x >> y >> t;
    Onde onde(x, y, t);
    std::cout << "La hauteur de la surface ondulee a (" << x << ", " << y << ", " << t << ") est " << onde.hauteur() << std::endl;

    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(-1000000000, 1000000000);
        x = dis(gen);
        y = dis(gen);
        t = dis(gen);
        Onde onde(x, y, t);
        std::cout << "La hauteur de la surface ondulee a (" << x << ", " << y << ", " << t << ") est " << onde.hauteur() << std::endl;
    }

    return 0;
}