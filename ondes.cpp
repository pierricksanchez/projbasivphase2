#include "ondes.hpp"
#include <cmath>
#include <cstdlib>
#include <random>

Onde::Onde(double x, double y, double t)
    : x_(x), y_(y), t_(t)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-10, 10);
    for (int n = 0; n < N; ++n) {
        A_[n] = dis(gen); // amplitude
        T_[n] = dis(gen); // période
        c_[n] = dis(gen); // position horizontale de la crête
        phi_[n] = dis(gen) * 2.0 * M_PI; // phase aléatoire
        theta_[n] = dis(gen) * 2.0 * M_PI; // angle de rotation aléatoire
    }
}

double Onde::hauteur() const
{
    double h = 0.0;
    for (int n = 0; n < N; ++n) {
        h += A_[n] * sin(2.0 * M_PI / T_[n] * (x_ * cos(theta_[n]) - y_ * sin(theta_[n]) - c_[n]) - phi_[n]);
    }
    return h;
}